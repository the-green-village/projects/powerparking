# EVTEC coffee&charge bi-directional (deprecated)

https://www.evtec.ch/

Documentation:
- [factsheet_coffeecharge_bidirectional_en.pdf](factsheet_coffeecharge_bidirectional_en.pdf)

```
curl -k -u "$USERNAME:$PASSWORD" \
  "https://support.evtec.ch/barista10/data?server=19030554_10843.0A&fields=consumerId,SOC,RemainingBatteryCapacity,TotalBatteryCapacity&count=10000&starttime=1585600000&endtime=1585686400"
```
