# -*- coding: utf-8 -*-
"""Connector between the EVTEC charger and The Green Village data platform.
The data is obtained from https://support.evtec.ch/barista10/data and sent using Kafka REST Proxy.
"""

import logging
import pathlib
import time
from configparser import ConfigParser
from datetime import datetime

import requests
from requests.exceptions import RequestException

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG
)
logger = logging.getLogger(__file__)

parent_dir = pathlib.Path(__file__).parent.resolve()

# Read config file.
config = ConfigParser()
config.read(str(parent_dir / "powerparking.cfg"))
evtec_username = config.get("Evtec", "username")
evtec_password = config.get("Evtec", "password")
rest_url = config.get("RestProxy", "url")
rest_username = config.get("RestProxy", "username")
rest_password = config.get("RestProxy", "password")
registry_url = config.get("SchemaRegistry", "url")

# Time interval between polling.
PERIOD = 60
# Time range obtained in an individual polling.
RANGE = 120

# Get schema from the schema registry.
r = requests.get(registry_url, timeout=5)
logger.debug(r.text)
logger.info(f"Kafka Schema Registry response code: {r.status_code}")
# value_schema_id = r.json()['id']
schema = r.json()["schema"]


while True:
    # time.sleep(PERIOD)

    # Get data from EVTEC.
    # The data is obtained as a list of JSON entries in the following format.
    # There are two consumers available, we are interested in 19030554_10843.0A
    # {
    #   "SOC": 100,
    #   "RemainingBatteryCapacity": 25500,
    #   "TotalBatteryCapacity": 25500,
    #   "consumerName": "19030554_10843.0A",
    #   "consumerConnected": 1,
    #   "firmware": "3.3rc7",
    #   "serverName": "19030554_10843.0A",
    #   "currentTimeStamp": "2020-03-12T15:33:43.000Z"
    # }
    try:
        endtime = int(time.time())
        starttime = endtime - RANGE
        url = (
            "https://support.evtec.ch/barista10/data?"
            + "server=19030554_10843.0A&"
            + "fields=consumerId,SOC,RemainingBatteryCapacity,TotalBatteryCapacity&"
            + "count=10000&"
            + f"starttime={starttime}&endtime={endtime}"
        )
        logger.debug(url)
        # Reserve enough time for this request, it is slow.
        # Disable the SSL verification, there seems to be a problem with the certificate.
        r = requests.get(
            url, auth=(evtec_username, evtec_password), timeout=30, verify=False
        )
        logger.debug(r.text)
        logger.info("Evtec response code: %s" % r.status_code)

    except RequestException as e:
        logger.error(e)
        continue

    if r.status_code != 200:
        continue
    datapoints = r.json()
    logger.info("Received %d data points." % len(datapoints))
    if len(datapoints) == 0:
        continue

    records = []
    for d in datapoints:
        # We are interested only in consumer 19030554_10843.0A
        if d["consumerName"] != "19030554_10843.0A":
            continue

        # Millisecond timestamp
        timestamp = (
            int(
                datetime.strptime(
                    d["currentTimeStamp"], "%Y-%m-%dT%H:%M:%S.%fZ"
                ).strftime("%s")
            )
            * 1000
        )

        value = {
            "value": {
                "metadata": {
                    "project_id": "powerparking",
                    "device_id": "EVTEC",
                    "description": "EVTEC charger",
                    "type": {"null": None},
                    "manufacturer": {"null": None},
                    "serial": {"null": None},
                    "placement_timestamp": {"null": None},
                    "location": {"null": None},
                    "latitude": {"null": None},
                    "longitude": {"null": None},
                    "altitude": {"null": None},
                },
                "data": {
                    "project_id": "powerparking",
                    "device_id": "EVTEC",
                    "timestamp": timestamp,
                    "values": [
                        {
                            "name": "SOC",
                            "description": "State of charge",
                            "unit": "%",
                            "type": "INT",
                            "value": {"int": d["SOC"]},
                        },
                        {
                            "name": "RemainingBatteryCapacity",
                            "description": "Remaining battery capacity",
                            "unit": "W",
                            "type": "INT",
                            "value": {"int": d["RemainingBatteryCapacity"]},
                        },
                        {
                            "name": "TotalBatteryCapacity",
                            "description": "Total battery capacity",
                            "unit": "Wh",
                            "type": "INT",
                            "value": {"int": d["TotalBatteryCapacity"]},
                        },
                    ],
                },
            }
        }
        logger.debug(value)
        records.append(value)

    data = {
        # "value_schema_id": value_schema_id,
        "value_schema": schema,
        "records": records,
    }

    try:
        headers = {
            "Content-Type": "application/vnd.kafka.avro.v2+json",
            "Accept": "application/vnd.kafka.v2+json",
        }
        r = requests.post(
            rest_url,
            json=data,
            headers=headers,
            auth=(rest_username, rest_password),
            timeout=5,
        )
        logger.info(r.text)
        logger.info(f"Kafka REST Proxy response code: {r.status_code}")

    except RequestException as e:
        logger.error(e)
        continue
