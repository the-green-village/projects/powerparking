#!/bin/bash
# mo powerparking.cfg_template > powerparking.cfg

kubectl delete configmap powerparking-ecs -n ccloud
kubectl create configmap powerparking-ecs -n ccloud \
  --from-file=powerparking.cfg=powerparking.cfg

kubectl delete deploy powerparking-ecs -n ccloud
kubectl apply -f powerparking-ecs-dep.yaml
