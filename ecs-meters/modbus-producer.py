# -*- coding: utf-8 -*-
"""Connector between the ECS meters and The Green Village digital platform.

The ECS meters are read out over Modbus TCP connection and
the data is sent to the Kafka using a Kafka producer (recommended).
"""

import json
import logging
import os
import pathlib
import time
from configparser import ConfigParser

from confluent_kafka import SerializingProducer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)

# Meters
devices = [
    {"device_id": "DC_charger", "device_description": "DC charger", "slave_id": 11},
    {"device_id": "PV", "device_description": "PV", "slave_id": 12},
    {"device_id": "AC_charger", "device_description": "AC charger", "slave_id": 13},
    {"device_id": "main_switch", "device_description": "main switch", "slave_id": 14},
    {"device_id": "microsens", "device_description": "microsens", "slave_id": 15},
]

# Quantities to read
measurements = [
    {
        "measurement_id": "Active Power L1",
        "measurement_description": "Active Power L1",
        "unit": "kW",
        "address": 4151,
    },
    {
        "measurement_id": "Active Power L2",
        "measurement_description": "Active Power L2",
        "unit": "kW",
        "address": 4153,
    },
    {
        "measurement_id": "Active Power L3",
        "measurement_description": "Active Power L3",
        "unit": "kW",
        "address": 4155,
    },
    {
        "measurement_id": "Active Power Sigma L",
        "measurement_description": "Active Power Sigma L",
        "unit": "kW",
        "address": 4157,
    },
    {
        "measurement_id": "Voltage L1-N",
        "measurement_description": "Voltage L1-N",
        "unit": "V",
        "address": 4267,
    },
    {
        "measurement_id": "Voltage L2-N",
        "measurement_description": "Voltage L2-N",
        "unit": "V",
        "address": 4269,
    },
    {
        "measurement_id": "Voltage L3-N",
        "measurement_description": "Voltage L3-N",
        "unit": "V",
        "address": 4271,
    },
    {
        "measurement_id": "Current L1",
        "measurement_description": "Current L1",
        "unit": "A",
        "address": 4279,
    },
    {
        "measurement_id": "Current L2",
        "measurement_description": "Current L2",
        "unit": "A",
        "address": 4281,
    },
    {
        "measurement_id": "Current L3",
        "measurement_description": "Current L3",
        "unit": "A",
        "address": 4283,
    },
    {
        "measurement_id": "Reactive Power L1",
        "measurement_description": "Reactive Power L1",
        "unit": "kvar",
        "address": 4257,
    },
    {
        "measurement_id": "Reactive Power L2",
        "measurement_description": "Reactive Power L2",
        "unit": "kvar",
        "address": 4259,
    },
    {
        "measurement_id": "Reactive Power L3",
        "measurement_description": "Reactive Power L3",
        "unit": "kvar",
        "address": 4261,
    },
    {
        "measurement_id": "Reactive Power Sigma L",
        "measurement_description": "Reactive Power Sigma L",
        "unit": "kvar",
        "address": 4263,
    },
    {
        "measurement_id": "Apparent Power L1",
        "measurement_description": "Apparent Power L1",
        "unit": "kVA",
        "address": 4285,
    },
    {
        "measurement_id": "Apparent Power L2",
        "measurement_description": "Apparent Power L2",
        "unit": "kVA",
        "address": 4287,
    },
    {
        "measurement_id": "Apparent Power L3",
        "measurement_description": "Apparent Power L3",
        "unit": "kVA",
        "address": 4289,
    },
    {
        "measurement_id": "Apparent Power Sigma L",
        "measurement_description": "Apparent Power Sigma L",
        "unit": "kVA",
        "address": 4291,
    },
    {
        "measurement_id": "cos phi L1",
        "measurement_description": "cos phi L1",
        "unit": "",
        "address": 4295,
    },
    {
        "measurement_id": "cos phi L2",
        "measurement_description": "cos phi L2",
        "unit": "",
        "address": 4297,
    },
    {
        "measurement_id": "cos phi L3",
        "measurement_description": "cos phi L3",
        "unit": "",
        "address": 4299,
    },
    {
        "measurement_id": "cos phi Sigma L",
        "measurement_description": "cos phi Sigma L",
        "unit": "",
        "address": 4301,
    },
]

if __name__ == "__main__":
    # Read config file.
    parent_dir = pathlib.Path(__file__).parent.resolve()
    config = ConfigParser()
    config.read(str(parent_dir / "powerparking.cfg"))
    modbus_host = config.get("Modbus", "host")
    modbus_port = config.getint("Modbus", "port")
    bootstrap_servers = config.get("Kafka", "bootstrap_servers")
    api_key = config.get("Kafka", "api_key")
    api_secret = config.get("Kafka", "api_secret")
    topic = config.get("Kafka", "topic")
    schema_registry_url = config.get("Kafka", "schema_registry_url")
    schema_registry_auth = config.get("Kafka", "schema_registry_auth")

    # Time between sensor readouts in seconds
    period = int(os.getenv("PERIOD", 1))
    logger.info("Period between data readout: %d seconds" % (period))

    conf = {"url": schema_registry_url, "basic.auth.user.info": schema_registry_auth}
    schema_registry_client = SchemaRegistryClient(conf)

    schema_str = '{"type":"record","name":"GreenVillageRecord","namespace":"thegreenvillage","fields":[{"name":"project_id","type":"string","doc":"Globally unique id for the project."},{"name":"application_id","type":"string","doc":"Unique id for the application/use case."},{"name":"device_id","type":"string","doc":"Unique id for the device."},{"name":"timestamp","type":"long","doc":"Timestamp (milliseconds since unix epoch) of the measurement."},{"name":"measurements","type":{"type":"array","items":{"type":"record","name":"measurement","fields":[{"name":"measurement_id","type":"string","doc":"Unique id for the measurement."},{"name":"value","type":["null","boolean","int","long","float","double","string"],"doc":"Measured value."},{"name":"unit","type":["null","string"],"doc":"Unit of the measurement.","default":null},{"name":"measurement_description","type":["null","string"],"doc":"Measurement description.","default":null}]}},"doc":"Array of measurements."},{"name":"project_description","type":["null","string"],"doc":"Project description.","default":null},{"name":"application_description","type":["null","string"],"doc":"Application/use case description.","default":null},{"name":"device_description","type":["null","string"],"doc":"Device description.","default":null},{"name":"device_manufacturer","type":["null","string"],"doc":"Device manufacturer.","default":null},{"name":"device_type","type":["null","string"],"doc":"Device type.","default":null},{"name":"device_serial","type":["null","string"],"doc":"Device serial number.","default":null},{"name":"location_id","type":["null","string"],"doc":"Unique id for the location.","default":null},{"name":"location_description","type":["null","string"],"doc":"Location description.","default":null},{"name":"latitude","type":["null","float"],"doc":"Latitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"longitude","type":["null","float"],"doc":"Longitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"altitude","type":["null","float"],"doc":"Altitude in meters above the mean sea level.","default":null}]}'
    conf = {"auto.register.schemas": False}
    avro_serializer = AvroSerializer(schema_str, schema_registry_client, conf=conf)

    # Create Producer instance
    producer = SerializingProducer(
        {
            "client.id": "powerparking-ecs-producer",
            "bootstrap.servers": bootstrap_servers,
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": api_key,
            "sasl.password": api_secret,
            # "ssl.ca.location": "/etc/ssl/certs/ca-certificates.crt",
            "value.serializer": avro_serializer,
            "acks": "1",
            # If you don’t care about duplicates and ordering:
            "retries": 10000000,
            "delivery.timeout.ms": 2147483647,
            "max.in.flight.requests.per.connection": 5
            #        # If you don’t care about duplicates but care about ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'max.in.flight.requests.per.connection': 1
            #        # If you care about duplicates and ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'enable.idempotence': True
        }
    )

    delivered_records = 0

    # Optional per-message on_delivery handler (triggered by poll() or flush())
    # when a message has been successfully delivered or
    # permanently failed delivery (after retries).
    def acked(err, msg):
        global delivered_records
        """Delivery report handler called on
        successful or failed delivery of message
        """
        if err is not None:
            print("Failed to deliver message: {}".format(err))
        else:
            delivered_records += 1
            print(
                "Produced record to topic {} partition [{}] @ offset {}".format(
                    msg.topic(), msg.partition(), msg.offset()
                )
            )

    # Modbus TCP client
    client = ModbusClient(modbus_host, port=modbus_port)

    try:
        while True:
            # Open or reconnect TCP to server.
            if not client.is_socket_open():
                if not client.connect():
                    logger.warning(f"Unable to connect to {modbus_host}:{modbus_port}")
                    time.sleep(5)
                    continue

            for d in devices:
                values = []
                for m in measurements:
                    # It is necessary to address the registers with offset -1.
                    address = m["address"] - 1
                    # All values are coded as 32bit floating point values.
                    res = client.read_holding_registers(
                        address, count=4, unit=d["slave_id"]
                    )
                    # TODO: Handle ModbusIOException.
                    logger.debug(
                        "Registers at address {}: {}".format(address, res.registers)
                    )
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        res.registers, byteorder=">", wordorder="<"
                    )
                    decoded = decoder.decode_32bit_float()
                    logger.debug("Decoded value: %s" % (decoded))
                    value = {
                        "measurement_id": m["measurement_id"],
                        "measurement_description": m["measurement_description"],
                        "unit": m["unit"],
                        "value": float(decoded),
                    }
                    logger.debug(value)
                    values.append(value)

                value = {
                    "project_id": "powerparking",
                    "application_id": "ecs",
                    "device_id": d["device_id"],
                    "device_description": d["device_description"],
                    "timestamp": int(time.time() * 1e3),  # time in ms
                    "measurements": values,
                }

                producer.produce(topic=topic, value=value, on_delivery=acked)
                # producer.poll(0)

            time.sleep(period)

    except KeyboardInterrupt:
        logger.error("Aborted by user")

    finally:
        producer.flush()

    print("{} messages were produced to topic {}!".format(delivered_records, topic))
