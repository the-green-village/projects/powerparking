# -*- coding: utf-8 -*-
"""Connector between the ECS meters and The Green Village digital platform.

The ECS meters are read out over Modbus TCP connection and
the data is sent using Kafka REST Proxy.
"""

import json
import logging
import os
import pathlib
import time
from configparser import ConfigParser

import requests
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder
from requests.exceptions import RequestException

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)

# Meters
devices = [
    {"device_id": "DC_charger", "description": "DC charger", "slave_id": 11},
    {"device_id": "PV", "description": "PV", "slave_id": 12},
    {"device_id": "AC_charger", "description": "AC charger", "slave_id": 13},
    {"device_id": "main_switch", "description": "main switch", "slave_id": 14},
    {"device_id": "microsens", "description": "microsens", "slave_id": 15},
]

# Quantities to read
measurements = [
    {
        "name": "Active Power L1",
        "description": "Active Power L1",
        "unit": "kW",
        "address": 4151,
    },
    {
        "name": "Active Power L2",
        "description": "Active Power L2",
        "unit": "kW",
        "address": 4153,
    },
    {
        "name": "Active Power L3",
        "description": "Active Power L3",
        "unit": "kW",
        "address": 4155,
    },
    {
        "name": "Active Power Sigma L",
        "description": "Active Power Sigma L",
        "unit": "kW",
        "address": 4157,
    },
    {
        "name": "Voltage L1-N",
        "description": "Voltage L1-N",
        "unit": "V",
        "address": 4267,
    },
    {
        "name": "Voltage L2-N",
        "description": "Voltage L2-N",
        "unit": "V",
        "address": 4269,
    },
    {
        "name": "Voltage L3-N",
        "description": "Voltage L3-N",
        "unit": "V",
        "address": 4271,
    },
    {"name": "Current L1", "description": "Current L1", "unit": "A", "address": 4279},
    {"name": "Current L2", "description": "Current L2", "unit": "A", "address": 4281},
    {"name": "Current L3", "description": "Current L3", "unit": "A", "address": 4283},
    {
        "name": "Reactive Power L1",
        "description": "Reactive Power L1",
        "unit": "kvar",
        "address": 4257,
    },
    {
        "name": "Reactive Power L2",
        "description": "Reactive Power L2",
        "unit": "kvar",
        "address": 4259,
    },
    {
        "name": "Reactive Power L3",
        "description": "Reactive Power L3",
        "unit": "kvar",
        "address": 4261,
    },
    {
        "name": "Reactive Power Sigma L",
        "description": "Reactive Power Sigma L",
        "unit": "kvar",
        "address": 4263,
    },
    {
        "name": "Apparent Power L1",
        "description": "Apparent Power L1",
        "unit": "kVA",
        "address": 4285,
    },
    {
        "name": "Apparent Power L2",
        "description": "Apparent Power L2",
        "unit": "kVA",
        "address": 4287,
    },
    {
        "name": "Apparent Power L3",
        "description": "Apparent Power L3",
        "unit": "kVA",
        "address": 4289,
    },
    {
        "name": "Apparent Power Sigma L",
        "description": "Apparent Power Sigma L",
        "unit": "kVA",
        "address": 4291,
    },
    {"name": "cos phi L1", "description": "cos phi L1", "unit": "", "address": 4295},
    {"name": "cos phi L2", "description": "cos phi L2", "unit": "", "address": 4297},
    {"name": "cos phi L3", "description": "cos phi L3", "unit": "", "address": 4299},
    {
        "name": "cos phi Sigma L",
        "description": "cos phi Sigma L",
        "unit": "",
        "address": 4301,
    },
]

if __name__ == "__main__":
    # Read config file.
    parent_dir = pathlib.Path(__file__).parent.resolve()
    config = ConfigParser()
    config.read(str(parent_dir / "powerparking.cfg"))
    modbus_host = config.get("Modbus", "host")
    modbus_port = config.getint("Modbus", "port")
    rest_url = config.get("RestProxy", "url")
    rest_username = config.get("RestProxy", "username")
    rest_password = config.get("RestProxy", "password")
    registry_url = config.get("RestProxy", "schema_registry_url")

    # Get schema from the schema registry.
    r = requests.get(registry_url, timeout=5)
    logger.debug(r.text)
    logger.info(f"Kafka Schema Registry response code: {r.status_code}")
    schema_id = r.json()["id"]
    schema = r.json()["schema"]

    # Modbus TCP client
    client = ModbusClient(modbus_host, port=modbus_port)

    while True:
        # Open or reconnect TCP to server.
        if not client.is_socket_open():
            if not client.connect():
                logger.warning(f"Unable to connect to {modbus_host}:{modbus_port}")
                time.sleep(5)
                continue

        records = []
        for d in devices:
            values = []
            for m in measurements:
                # It is necessary to address the registers with offset -1.
                address = m["address"] - 1
                # All values are coded as 32bit floating point values.
                res = client.read_holding_registers(
                    address, count=4, unit=d["slave_id"]
                )
                # TODO: Handle ModbusIOException.
                logger.debug(
                    "Registers at address {}: {}".format(address, res.registers)
                )
                decoder = BinaryPayloadDecoder.fromRegisters(
                    res.registers, byteorder=">", wordorder="<"
                )
                decoded = decoder.decode_32bit_float()
                logger.debug("Decoded value: %s" % (decoded))
                value = {
                    "name": m["name"],
                    "description": m["description"],
                    "unit": m["unit"],
                    "type": "FLOAT",
                    "value": {"float": decoded},
                }
                logger.debug(value)
                values.append(value)

            value = {
                "value": {
                    "metadata": {
                        "project_id": "powerparking",
                        "device_id": d["device_id"],
                        "description": d["description"],
                        "type": {"null": None},
                        "manufacturer": {"null": None},
                        "serial": {"null": None},
                        "placement_timestamp": {"null": None},
                        "location": {"null": None},
                        "latitude": {"null": None},
                        "longitude": {"null": None},
                        "altitude": {"null": None},
                    },
                    "data": {
                        "project_id": "powerparking",
                        "device_id": d["device_id"],
                        "timestamp": int(time.time() * 1e3),  # time in ms
                        "values": values,
                    },
                }
            }
            records.append(value)

        data = {
            "value_schema_id": schema_id,
            # "value_schema": schema,
            "records": records,
        }

        try:
            headers = {
                "Content-Type": "application/vnd.kafka.avro.v2+json",
                "Accept": "application/vnd.kafka.v2+json",
            }
            r = requests.post(
                rest_url,
                json=data,
                headers=headers,
                auth=(rest_username, rest_password),
                timeout=5,
            )
            logger.info(r.text)
            logger.info("Kafka REST Proxy response code: %d" % (r.status_code))

        except RequestException as e:
            logger.error(e)
            continue

        time.sleep(1)
