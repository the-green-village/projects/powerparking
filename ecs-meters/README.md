# ECS meters

The ECS meters are read out over Modbus TCP connection.

Documentation:
- [ECSLS04_LAN_modbus_Mnl_UK.pdf](ECSLS04_LAN_modbus_Mnl_UK.pdf)
- [021_modbus_protocol_technical_description.pdf](021_modbus_protocol_technical_description.pdf)
http://qualen.ru/files/files/1/4/392/021_modbus_protocol_technical_description.pdf

## mbpoll

mbpoll is a command line utility to communicate with Modbus slave.

https://github.com/epsilonrt/mbpoll

This is an example of reading Active Power Sigma (register 4157) from the DC Charger (slave ID 11):

```
mbpoll -p 502 145.94.45.204 -a 11 -r 4157 -t 4:float
```

## PyModbus

https://pymodbus.readthedocs.io/en/v2.1.0/index.html

```
pip3 install pymodbus
```

## Producing to Kafka

Three python scripts for producing to Kafka are available in this repository:
- [modbus-producer.py](modbus-producer.py) sends data obtained via Modbus using a Kafka producer (preferred way, more performant),
- [modbus-rest.py](modbus-rest.py) sends data obtained via Modbus to the Kafka REST Proxy.

### Run on Raspberry Pi

To run on startup, add the following line to `/etc/rc.local`

```
#/usr/bin/python3 /home/pi/powerparking/ecs-meters/modbus-rest.py 1>/dev/null 2>&1 &
#/usr/bin/python3 /home/pi/powerparking/ecs-meters/modbus-rest.py 1>/home/pi/powerparking/ecs-meters/modbus-rest.log 2>&1 &
/home/pi/powerparking/ecs-meters/start.sh &
```

For testing purposes, you can run the [modbus-rest.py](modbus-rest.py) script locally or using Docker.

### Run with Docker

```
docker build . -t powerparking-ecs
docker tag powerparking-ecs registry.utility.sda-projects.nl/sda/powerparking-ecs:2.0.0
docker push registry.utility.sda-projects.nl/sda/powerparking-ecs:2.0.0

# docker run --mount type=bind,source=`pwd`/powerparking.cfg,target=/powerparking.cfg,readonly powerparking-ecs python modbus-rest.py
# docker run --mount type=bind,source=`pwd`/powerparking.cfg,target=/powerparking.cfg,readonly powerparking-ecs python modbus-producer.py
docker run -v $(pwd)/powerparking.cfg:/powerparking.cfg powerparking-ecs
```

### Deploy on Kubernetes

```
# Create ConfigMap with the configuration file.
kubectl delete configmap powerparking-ecs -n ccloud
kubectl create configmap powerparking-ecs -n ccloud \
  --from-file=powerparking.cfg=powerparking.cfg

# Deploy a pod.
kubectl delete deploy powerparking-ecs -n ccloud
kubectl apply -f powerparking-ecs-dep.yaml

kubectl logs deploy/powerparking-ecs -n ccloud
```
