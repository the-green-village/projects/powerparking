# Power Parking

The Power Parking project connects the following sensors to The Green Village digital platform:
- Bosch Parking Lot Sensors using LoRaWAN in [bosch-parking-sensor](bosch-parking-sensor) (no longer active)
- Energy meters using Modbus in [ecs-meters](ecs-meters) (no longer active)
- Pyranometer in [solar-intensity](solar-intensity)
- CO2 data in [co2](co2)
