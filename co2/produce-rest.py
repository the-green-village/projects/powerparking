# -*- coding: utf-8 -*-
import pathlib
import time
from configparser import ConfigParser
from pprint import pformat

import requests

if __name__ == "__main__":
    # Read config file
    config = ConfigParser()
    config.read(str(pathlib.Path(__file__).parent.resolve() / "co2.cfg"))
    rest_proxy_url = config.get("RestProxy", "url")
    api_key = config.get("RestProxy", "username")
    api_secret = config.get("RestProxy", "password")
    schema_registry_url = config.get("RestProxy", "schema_registry_url")
    co2signal_url = config.get("CO2", "url")
    co2signal_auth_token = config.get("CO2", "auth_token")
    ci_marg_url = config.get("CO2", "ci_marg_url")
    ci_marg_auth_token = config.get("CO2", "auth_token2")

    # Get the schema from the Schema Registry.
    r = requests.get(schema_registry_url)
    print(r.status_code)
    schema_id = r.json()["id"]
    schema_str = r.json()["schema"]
    print(schema_str)

    timestamp = int(time.time() * 1e3)  # time in ms

    data = {
        "value_schema_id": schema_id,
        # "value_schema": schema_str,
        "records": [],
    }

    try:
        r = requests.get(co2signal_url, headers={"auth-token": co2signal_auth_token})
        print(r.status_code)
        print(pformat(r.json()))

        carbon_intensity = r.json()["data"]["carbonIntensity"]
        fossil_fuel_percent = r.json()["data"]["fossilFuelPercentage"]

        measurements_co2signal = [
            {
                "measurement_id": "carbonIntensity",
                "measurement_description": {"string": "Dutch carbon intensity"},
                "unit": {"string": "gCO2eq/kWh"},
                "value": {"float": float(carbon_intensity)},
            },
            {
                "measurement_id": "fossilFuelPercentage",
                "measurement_description": {"string": "Energy share fossil fuels"},
                "unit": {"string": "%"},
                "value": {"float": float(fossil_fuel_percent)},
            },
        ]

        value_co2signal = {
            "project_id": "powerparking",
            "application_id": "CO2",
            "device_id": "co2signal",
            "timestamp": timestamp,
            "measurements": measurements_co2signal,
            "project_description": None,
            "application_description": None,
            "device_description": None,
            "device_type": None,
            "device_manufacturer": None,
            "device_serial": None,
            "location_id": None,
            "location_description": None,
            "latitude": None,
            "longitude": None,
            "altitude": None,
        }

        data["records"].append({"value": value_co2signal})

    except Exception as e:
        print(repr(e))

    try:
        r = requests.get(ci_marg_url, headers={"auth-token": ci_marg_auth_token})
        print(r.status_code)
        print(pformat(r.json()))

        marg_carbon_intensity = r.json()["forecast"][0]["marginalCarbonIntensity"]

        measurements_electricitymap = [
            {
                "measurement_id": "marginalCarbonIntensity",
                "measurement_description": {
                    "string": "Carbon intensity of marginal powerplant"
                },
                "unit": {"string": "gCO2eq/kWh"},
                "value": {"float": float(marg_carbon_intensity)},
            }
        ]

        value_electricitymap = {
            "project_id": "powerparking",
            "application_id": "CO2",
            "device_id": "electricitymap",
            "timestamp": timestamp,
            "measurements": measurements_electricitymap,
            "project_description": None,
            "application_description": None,
            "device_description": None,
            "device_type": None,
            "device_manufacturer": None,
            "device_serial": None,
            "location_id": None,
            "location_description": None,
            "latitude": None,
            "longitude": None,
            "altitude": None,
        }

        data["records"].append({"value": value_electricitymap})

    except Exception as e:
        print(repr(e))

    headers = {
        "Content-Type": "application/vnd.kafka.avro.v2+json",
        "Accept": "application/vnd.kafka.v2+json",
    }
    r = requests.post(
        rest_proxy_url, json=data, headers=headers, auth=(api_key, api_secret)
    )
    # print(r.status_code)
    print(r.text)
