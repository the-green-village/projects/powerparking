# CO2

The data from https://www.co2signal.com/ is read from a Raspberry Pi.

## Raspberry Pi

The Raspberry Pi can be accessed from within a ZeroTier network.

```
ssh pi@10.242.72.41
```

Define a cron job to read the irradiation periodically.

```
crontab -e
```

```
0,30 * * * * /usr/bin/python3 /home/pi/powerparking/co2/produce-rest.py > /home/pi/powerparking/co2/cronjob.log 2>&1
# 0,30 * * * * /usr/bin/python3 /home/pi/powerparking/co2/serializing-producer.py > /home/pi/powerparking/co2/cronjob.log 2>&1
```
