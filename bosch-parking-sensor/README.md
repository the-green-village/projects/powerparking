# Bosch Parking Lot Sensor

The **Bosch Parking Lot Sensor** communicates over **The Things Network**, a global open **LoRaWAN** network.

Links and documentation:
- [parking-lot-sensor-user-manual-en.pdf](parking-lot-sensor-user-manual-en.pdf)
https://www.bosch-connectivity.com/media/product_detail_pls/parking-lot-sensor-user-manual-en.pdf
- The Things Network: https://www.thethingsnetwork.org/
- TTN console: https://console.thethingsnetwork.org/applications/powerparking

## HTTP integration

The messages from the **Bosch Parking Sensor** are sent using the **HTTP integration** to a serverless function deployed in The Green Village data platform:
https://gitlab.com/the-green-village/tgv-functions/-/tree/master/ttn-bosch-parking

This decoder function for The Things Network to unpack the payload of Bosch Parking Lot Sensors is used:
https://github.com/SensationalSystems/bosch-parking-lot-sensor-decoder/blob/master/decoder.js

## MQTT

Python application SDK for TTN allows you to send and receive messages to and from IoT devices over MQTT.

https://www.thethingsnetwork.org/docs/applications/python/

# TTN data storage

https://powerparking.data.thethingsnetwork.org/

```
curl -X GET --header 'Accept: application/json' --header "Authorization: key $ACCESS_KEY" 'https://powerparking.data.thethingsnetwork.org/api/v2/devices'

curl -X GET --header 'Accept: application/json' --header "Authorization: key $ACCESS_KEY" 'https://powerparking.data.thethingsnetwork.org/api/v2/query'
```
