# Pyranometer

The data from the pyranometer is read using a Raspberry Pi with a dedicated python code to communicate with the sensors.

## Raspberry Pi

### ZeroTier

The Raspberry Pi can be accessed from within a ZeroTier network.

```
ssh pi@10.242.72.41
```

### confluent-kafka and librdkakfa

The `confluent-kafka` python package needs `librdkafka`.

http://mirrordirector.raspbian.org/raspbian/pool/main/libr/librdkafka/

```
sudo apt-get install libssl-dev libsasl2-dev

wget http://mirrordirector.raspbian.org/raspbian/pool/main/libr/librdkafka/librdkafka_1.6.0.orig.tar.gz
tar xvzf librdkafka_1.6.0.orig.tar.gz
cd librdkafka-1.6.0
./configure
make
sudo make install
sudo ldconfig

sudo pip3 install confluent-kafka[avro,json,protobuf]==1.6.0
```

### Cron job

Define a cron job to read the irradiation periodically.

```
crontab -e
```

```
* * * * * /usr/bin/python3 /home/pi/powerparking/solar-intensity/modbus-rest.py > /home/pi/powerparking/solar-intensity/cronjob.log 2>&1
# * * * * * /usr/bin/python3 /home/pi/powerparking/solar-intensity/serializing-producer.py > /home/pi/powerparking/solar-intensity/cronjob.log 2>&1
```
