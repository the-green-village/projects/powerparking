# -*- coding: utf-8 -*-
"""Connector between the pyranometer and The Green Village digital platform.

This script uses the custom code for the pyranometer available on the Raspberry Pi.
"""

import sys

sys.path.append("/home/pi/Desktop/Software Package")
import logging
import pathlib
import time
from configparser import ConfigParser

import requests
from Functions import *
from requests.exceptions import RequestException

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG
)
logger = logging.getLogger(__file__)

parent_dir = pathlib.Path(__file__).parent.resolve()

# Read config file
config = ConfigParser()
config.read(str(parent_dir / "powerparking.cfg"))
rest_url = config.get("RestProxy", "url")
rest_username = config.get("RestProxy", "username")
rest_password = config.get("RestProxy", "password")
registry_url = config.get("RestProxy", "schema_registry_url")

# Get schema from the schema registry.
r = requests.get(registry_url, timeout=5)
logger.debug(r.text)
logger.info("Kafka Schema Registry response code: %d" % r.status_code)
schema_id = r.json()["id"]
schema_str = r.json()["schema"]

irr = read_irradiation()

measurements = [
    {
        "measurement_id": "irradiation",
        "measurement_description": {"string": "Global horizontal irradiation"},
        "unit": {"string": "W/m2"},
        "value": {"float": float(irr)},
    }
]

value = {
    "project_id": "powerparking",
    "application_id": "weather",
    "device_id": "pyranometer",
    "timestamp": int(time.time() * 1e3),  # time in ms
    "measurements": measurements,
    "project_description": None,
    "application_description": None,
    "device_description": None,
    "device_type": None,
    "device_manufacturer": None,
    "device_serial": None,
    "location_id": None,
    "location_description": None,
    "latitude": None,
    "longitude": None,
    "altitude": None,
}
logger.debug(value)

data = {
    "value_schema_id": schema_id,
    # "value_schema": schema_str,
    "records": [{"value": value}],
}


headers = {
    "Content-Type": "application/vnd.kafka.avro.v2+json",
    "Accept": "application/vnd.kafka.v2+json",
}
r = requests.post(
    rest_url, json=data, headers=headers, auth=(rest_username, rest_password), timeout=5
)
logger.debug(r.text)
logger.info("Kafka REST Proxy response code: %d" % r.status_code)
