# -*- coding: utf-8 -*-
"""Connector between the pyranometer and The Green Village digital platform.

This script uses the custom code for the pyranometer available on the Raspberry Pi.
"""

import sys

sys.path.append("/home/pi/Desktop/Software Package")
import pathlib
from configparser import ConfigParser
from time import time

from confluent_kafka import SerializingProducer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from Functions import *

if __name__ == "__main__":
    parent_dir = pathlib.Path(__file__).parent.resolve()

    # Read config file
    config = ConfigParser()
    config.read(str(parent_dir / "powerparking.cfg"))
    bootstrap_servers = config.get("Kafka", "bootstrap_servers")
    api_key = config.get("Kafka", "api_key")
    api_secret = config.get("Kafka", "api_secret")
    topic = config.get("Kafka", "topic")
    schema_registry_url = config.get("Kafka", "schema_registry_url")
    schema_registry_auth = config.get("Kafka", "schema_registry_auth")

    conf = {"url": schema_registry_url, "basic.auth.user.info": schema_registry_auth}
    schema_registry_client = SchemaRegistryClient(conf)

    schema_str = '{"type":"record","name":"GreenVillageRecord","namespace":"thegreenvillage","fields":[{"name":"project_id","type":"string","doc":"Globally unique id for the project."},{"name":"application_id","type":"string","doc":"Unique id for the application/use case."},{"name":"device_id","type":"string","doc":"Unique id for the device."},{"name":"timestamp","type":"long","doc":"Timestamp (milliseconds since unix epoch) of the measurement."},{"name":"measurements","type":{"type":"array","items":{"type":"record","name":"measurement","fields":[{"name":"measurement_id","type":"string","doc":"Unique id for the measurement."},{"name":"value","type":["null","boolean","int","long","float","double","string"],"doc":"Measured value."},{"name":"unit","type":["null","string"],"doc":"Unit of the measurement.","default":null},{"name":"measurement_description","type":["null","string"],"doc":"Measurement description.","default":null}]}},"doc":"Array of measurements."},{"name":"project_description","type":["null","string"],"doc":"Project description.","default":null},{"name":"application_description","type":["null","string"],"doc":"Application/use case description.","default":null},{"name":"device_description","type":["null","string"],"doc":"Device description.","default":null},{"name":"device_manufacturer","type":["null","string"],"doc":"Device manufacturer.","default":null},{"name":"device_type","type":["null","string"],"doc":"Device type.","default":null},{"name":"device_serial","type":["null","string"],"doc":"Device serial number.","default":null},{"name":"location_id","type":["null","string"],"doc":"Unique id for the location.","default":null},{"name":"location_description","type":["null","string"],"doc":"Location description.","default":null},{"name":"latitude","type":["null","float"],"doc":"Latitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"longitude","type":["null","float"],"doc":"Longitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"altitude","type":["null","float"],"doc":"Altitude in meters above the mean sea level.","default":null}]}'
    conf = {"auto.register.schemas": False}
    avro_serializer = AvroSerializer(schema_str, schema_registry_client, conf=conf)

    # Create Producer instance
    producer = SerializingProducer(
        {
            "client.id": "powerparking-solar-intensity-producer",
            "bootstrap.servers": bootstrap_servers,
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": api_key,
            "sasl.password": api_secret,
            "ssl.ca.location": "/etc/ssl/certs/ca-certificates.crt",
            "value.serializer": avro_serializer,
            "acks": "1",
            # If you don’t care about duplicates and ordering:
            "retries": 10000000,
            "delivery.timeout.ms": 2147483647,
            "max.in.flight.requests.per.connection": 5
            #        # If you don’t care about duplicates but care about ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'max.in.flight.requests.per.connection': 1
            #        # If you care about duplicates and ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'enable.idempotence': True
        }
    )

    irr = read_irradiation()
    print(irr)

    measurements = [
        {
            "measurement_id": "irradiation",
            "measurement_description": "Global horizontal irradiation",
            "unit": "W/m2",
            "value": float(irr),
        }
    ]

    value = {
        "project_id": "powerparking",
        "application_id": "weather",
        "device_id": "pyranometer",
        "timestamp": int(time() * 1e3),  # time in ms
        "measurements": measurements,
    }

    producer.produce(topic=topic, value=value)
    producer.poll(0)

    producer.flush()
